<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::post('demotwo', 'DemoController@demotwo');
Route::match(['get', 'post'], 'demothree', 'DemoController@demothree');
Route::any('demofour', 'DemoController@demofour');

Route::get('demonine/{id}/{name}', function ($id, $name) {
    return 'demonine ID: '.$id.' || NAME: '.$name;
})->where(['id' => '[0-9]+', 'name' => '[a-z]+']);

Route::prefix('admin')->group(function () {
    Route::match(['get', 'post'], 'demothree2', 'DemoController@demothree');
    Route::any('demofour2', 'DemoController@demofour');
});

Route::resource('photos', 'PhotoController');
//Route::resource('admin/user', 'Admin\UsersController');

Route::prefix('admin')->middleware('auth')->group(function () {
    Route::resource('user', 'Admin\UsersController');
    Route::get('demoone', 'DemoController@index');
});

Route::get('login', 'LoginController@index')->name('login');
Route::get('logout', 'LoginController@logout');
Route::post('login', 'LoginController@authenticate');
